# db-ip-miner
MineMeld Miner for DB-IP table implemented as an extension

## How it works

This simple Miner periodically checks a DB-IP URL (https://db-ip.com/{as-list}) for new IP's in the table.

## Installation

You can install this extension directly from the git repo
